class Person < ApplicationRecord
  validates :name, :phone, presence: true
end
