class PersonsController < ApplicationController
  def index
    @persons = Person.all
    render json: @persons
  end

  def create
    @person = Person.new(person_params)
    if @person.save
      render json: @person
    else
      render json: @person.errors, status: 422
    end
  end

  def update
    @person = Person.find(params[:id])
    if @person.update(person_params)
      render json: @person
    else
      render json: @person.errors, status: 422
    end
  end

  private
    def person_params
      params.require(:person).permit(
        :name,
        :phone
      )
    end
end
